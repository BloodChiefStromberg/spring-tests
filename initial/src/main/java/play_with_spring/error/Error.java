package play_with_spring.error;

public class Error {
    private final String message;
    private final ErrorType type;

    public enum ErrorType {
        NOT_FOUND, INTERNAL_SERVER_ERROR, OTHER
    }

    public Error(ErrorType type, String message) {
        this.message = message;
        this.type = type;
    }

    // Think I may need to use the bean annotation to get this to register correctly
    public static Error getError(ErrorType type, String message) {
        return new Error(type, message);
    }

    public String getMessage() {
        return message;
    }

    public ErrorType getType() {
        return type;
    }
}
