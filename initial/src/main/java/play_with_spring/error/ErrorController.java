package play_with_spring.error;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    @RequestMapping("/special-error")
    public Error returnError() {
        return Error.getError(Error.ErrorType.NOT_FOUND, "Wasn't found :(");
    }
}
