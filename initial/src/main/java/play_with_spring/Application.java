package play_with_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/* This annotation includes:
    - @Configuration (this tags the class as a source of bean defs for app context)
    - @EnableAutoConfiguration starts adding beans based on CP and other shit
    - @EnableWebMVC is not needed -- spring boot helps us out by applying this when it sees spring-webmvc on the CP
    - @ComponentScan looks for other components -- like the controller we made in pkg:play_with_spring.hello!
*/
@SpringBootApplication
public class Application {
    public static void main (String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
