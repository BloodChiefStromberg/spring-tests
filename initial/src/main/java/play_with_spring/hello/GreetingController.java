package play_with_spring.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

// RestController is just one type of controller in Spring -- MVC is another
@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    // With a RestController, we can just directly return a representation object; Spring and Jackson2 will convert to JSON and HTTP for us
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue ="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                String.format(template, name));
    }
}
