package play_with_spring.hello;

// This is the representation class! It's not the actual data, just the representation class
// for the client to receive
public class Greeting {

    private final long id;
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
